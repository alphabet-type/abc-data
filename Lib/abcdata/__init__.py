"""abcData © 2021 by Alphabet Type GmbH"""

__version__ = "0.0.1" # Automatically bumped by bump2version


def get_package_version():
    return __version__
