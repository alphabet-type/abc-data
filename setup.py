from setuptools import setup, find_packages


setup(
    name="abcdata",
    version="0.0.2", # Automatically bumped by bump2version
    author="Alphabet Type GmbH",
    author_email="hello@alphabet-type.com",
    description="Installable Single Source of Truth for Alphabet Data",
    url="https://gitlab.com/alphabet-type/abc-data",
    platform=["Any"],
    package_dir={"": "Lib"},
    packages=find_packages("Lib"),
    package_data={
        "": [
            "templates/*.html",
            "templates/*/*.html",
            "templates/*/*.js",
            "templates/*/*.css",
            "tests/fonts/*.otf",
            "tests/fonts/*.woff"
        ]
    }
)
