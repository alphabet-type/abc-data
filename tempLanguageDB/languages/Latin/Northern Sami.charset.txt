
# Character Set Definition
#
# Name: Northern Sami
# Version: Version 1.0 2023
# Copyright: (c) 2023
# Description: Language definition for Northern Sami


# Basic Latin
0x0041	# LATIN CAPITAL LETTER A
0x0042	# LATIN CAPITAL LETTER B
0x0043	# LATIN CAPITAL LETTER C
0x0044	# LATIN CAPITAL LETTER D
0x0045	# LATIN CAPITAL LETTER E
0x0046	# LATIN CAPITAL LETTER F
0x0047	# LATIN CAPITAL LETTER G
0x0048	# LATIN CAPITAL LETTER H
0x0049	# LATIN CAPITAL LETTER I
0x004A	# LATIN CAPITAL LETTER J
0x004B	# LATIN CAPITAL LETTER K
0x004C	# LATIN CAPITAL LETTER L
0x004D	# LATIN CAPITAL LETTER M
0x004E	# LATIN CAPITAL LETTER N
0x004F	# LATIN CAPITAL LETTER O
0x0050	# LATIN CAPITAL LETTER P
0x0052	# LATIN CAPITAL LETTER R
0x0053	# LATIN CAPITAL LETTER S
0x0054	# LATIN CAPITAL LETTER T
0x0055	# LATIN CAPITAL LETTER U
0x0056	# LATIN CAPITAL LETTER V
0x005A	# LATIN CAPITAL LETTER Z
0x0061	# LATIN SMALL LETTER A
0x0062	# LATIN SMALL LETTER B
0x0063	# LATIN SMALL LETTER C
0x0064	# LATIN SMALL LETTER D
0x0065	# LATIN SMALL LETTER E
0x0066	# LATIN SMALL LETTER F
0x0067	# LATIN SMALL LETTER G
0x0068	# LATIN SMALL LETTER H
0x0069	# LATIN SMALL LETTER I
0x006A	# LATIN SMALL LETTER J
0x006B	# LATIN SMALL LETTER K
0x006C	# LATIN SMALL LETTER L
0x006D	# LATIN SMALL LETTER M
0x006E	# LATIN SMALL LETTER N
0x006F	# LATIN SMALL LETTER O
0x0070	# LATIN SMALL LETTER P
0x0072	# LATIN SMALL LETTER R
0x0073	# LATIN SMALL LETTER S
0x0074	# LATIN SMALL LETTER T
0x0075	# LATIN SMALL LETTER U
0x0076	# LATIN SMALL LETTER V
0x007A	# LATIN SMALL LETTER Z

# Latin-1 Supplement
0x00C1	# LATIN CAPITAL LETTER A WITH ACUTE
0x00E1	# LATIN SMALL LETTER A WITH ACUTE

# Latin Extended-A
0x010C	# LATIN CAPITAL LETTER C WITH CARON
0x010D	# LATIN SMALL LETTER C WITH CARON
0x0110	# LATIN CAPITAL LETTER D WITH STROKE
0x0111	# LATIN SMALL LETTER D WITH STROKE
0x014A	# LATIN CAPITAL LETTER ENG
0x014B	# LATIN SMALL LETTER ENG
0x0160	# LATIN CAPITAL LETTER S WITH CARON
0x0161	# LATIN SMALL LETTER S WITH CARON
0x0166	# LATIN CAPITAL LETTER T WITH STROKE
0x0167	# LATIN SMALL LETTER T WITH STROKE
0x017D	# LATIN CAPITAL LETTER Z WITH CARON
0x017E	# LATIN SMALL LETTER Z WITH CARON
