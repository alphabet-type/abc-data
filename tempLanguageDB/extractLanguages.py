"""
Maintenance of the language DataBase
"""
from __future__ import annotations

import logging
import os
import unicodedata2
from fontPartTools.unicodeinfo import getDecomposition
from abcdata.languages_data import language_db
from fontRepertoire.characterSet import CharSet

log = logging.getLogger(__name__)

selfFolder = os.path.dirname(os.path.realpath(__file__))
languagesFolderPath = os.path.join(selfFolder, "languages")


def getScriptFolderPath(scriptName: str) -> str:
    return os.path.join(languagesFolderPath, scriptName)


def getCharSetPath(scriptName: str, languageName: str) -> str:
    return os.path.join(getScriptFolderPath(scriptName), f"{languageName}.charset.txt")


def exportLanguages():
    if not os.path.isdir(languagesFolderPath):
        os.makedirs(languagesFolderPath)
    for scriptName, languages in language_db.items():
        scriptFolderPath = getScriptFolderPath(scriptName)
        if not os.path.isdir(scriptFolderPath):
            os.makedirs(scriptFolderPath)
        for languageName, codepoints in languages.items():
            charSet = CharSet(codepoints, languageName)
            charSet.description = f"Language definition for {languageName}"
            charSet.save(getCharSetPath(scriptName, languageName))

def writeLanguages_data():
    new_language_db = {}
    for name in os.listdir(languagesFolderPath):
        pathName = os.path.join(languagesFolderPath, name)
        if os.path.isdir(pathName):
            scriptName = name
            for _name in os.listdir(pathName):
                if _name.endswith(".charset.txt"):
                    charsetPath = os.path.join(pathName, _name)
                    if os.path.isfile(charsetPath):
                        charSet = CharSet.fromFile(charsetPath)
                        if scriptName not in new_language_db:
                            new_language_db[scriptName] = {}
                        new_language_db[scriptName][charSet.name] = list(sorted(charSet))
    new_language_db_path = os.path.join(selfFolder, "languages_data.py")
    with open(new_language_db_path, "w", encoding="utf-8") as new_language_db_file:
        new_language_db_file.write("language_db =")
        new_language_db_file.write(str(new_language_db))

def filterOutIsolatedPresentationForms(charSet:CharSet):
    isolatedPresentationForms = CharSet()
    for codepoint in sorted(charSet):
        character = chr(codepoint)
        cps, decompType = getDecomposition(character)
        if len(cps) == 1 and decompType == "isolated":
            cp = cps[0]
            if cp in charSet:
                print(f"{codepoint:04X} {unicodedata2.name(character)} {getDecomposition(character)}")
                isolatedPresentationForms.add(codepoint)
    charSet -= isolatedPresentationForms

def filterOutArabicIsolatedPresentationForms():
    scriptFolderPath = getScriptFolderPath("Arabic")
    for name in os.listdir(scriptFolderPath):
        if name.endswith(".charset.txt"):
            charsetPath = os.path.join(scriptFolderPath, name)
            charSet = CharSet.fromFile(charsetPath)
            filterOutIsolatedPresentationForms(charSet)
            charSet.save(charsetPath)


def main():
    # exportLanguages()
    filterOutArabicIsolatedPresentationForms()
    writeLanguages_data()


if __name__ == "__main__":
    main()
