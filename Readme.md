# Alphabet Data (abcdata)

Public Repo for to be used as _the_ data source for internal apps.

## Reasoning
In the past internal apps had some data pickles embedded, to be used as data source.
While this made the apps modular, it caused inconsistencies as every app had their own data source.

This repository is meant to solve this and should be referred as installable dependency in other internal packages.
